/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.19-MariaDB : Database - paz
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`paz` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `paz`;

/*Table structure for table `perfis` */

DROP TABLE IF EXISTS `perfis`;

CREATE TABLE `perfis` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `perfil` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `perfis` */

insert  into `perfis`(`id`,`perfil`) values 
(1,'ADMIN'),
(2,'USER');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nome completo',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'login do usuario',
  `contato` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Telefone de contato',
  `senha` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'senha para acesso',
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'gravatar do usuario',
  `perfil` int(4) unsigned NOT NULL COMMENT 'Perfil de acesso',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_email` (`email`) COMMENT 'chave representa login de usuario'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`nome`,`email`,`contato`,`senha`,`avatar`,`perfil`) values 
(1,'Thiago Braga Nobre','thiagobraganobre@gmail.com','(21) 98902-9472','e10adc3949ba59abbe56e057f20f883e','https://s.gravatar.com/avatar/a7d0870b3bbad3571970034167cea6df?s=80',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

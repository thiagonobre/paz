<?php
namespace Test;

class Simple  {

    function assertTrue($x)
    {
        if ($x) return true;
        return false;
    }

    function assertFalse($x)
    {
        if (!$x) return true;
        return false;
    }

    function assertNull($x)
    {
        if (empty($x)) return true;
        return false;
    }


    function assertNotNull($x)
    {
        if (!empty($x)) return true;
        return false;
    }

    function assertEqual($x, $y)
    {
        if ($x === $y) return true;
        return false;
    }

}

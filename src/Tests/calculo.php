<?php
namespace Test;
Use Test\Simple;
Use Service\Index;

class Calculo extends Simple{

    public function run()
    {
        $teste = new Index();
        $resp = $teste->calcular(1,2);
        return $this->assertEqual($resp,3);
    }    


}
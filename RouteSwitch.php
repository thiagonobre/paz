<?php
abstract class RouteSwitch
{
    public function __call($name, $arguments)
    {
        if (file_exists($GLOBALS['web']['realpath']."/src/Routes/$name.php"))
        {
            $request = $arguments;
            require $GLOBALS['web']['realpath']."/src/Routes/$name.php";
        }else{
            http_response_code(404);
            require __DIR__ . '/src/Routes/404.php';
        }
    }
}

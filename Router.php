<?php
require_once __DIR__ . '/RouteSwitch.php';

class Router extends RouteSwitch
{
    private $requestUri;

    function __construct() {
        //parent::__construct();
        $this->requestUri = $_SERVER['REQUEST_URI'];
    }


    
    public function run()
    {
        $route = substr($this->requestUri, 1);
        $route = str_replace($GLOBALS['web']['project'],'',$route);
        $route = str_replace('/','',$route);

        $request = $_SERVER['QUERY_STRING'];
        $data = null;
        if (!empty($request))
        {
            $route = str_replace("?","",$route);
            $route = str_replace($request,"",$route);
            $parametes = explode('&',$request);
            foreach($parametes as $k=>$v)
            {
                $item = explode("=",$v);
                $data[$item[0]]=$item[1];
            }
    
        }
        
        if ($route === 'auth') {
            $this->login();
        } else {
            $this->$route($data);

        }
    }
}
<?php
require_once __DIR__ . '/Config/web.php';

if ($GLOBALS['web']["debug"])
{
    ini_set('display_errors', 1);
    error_reporting(E_ALL);    
}
require_once "vendor/autoload.php";
require_once __DIR__ . '/Router.php';


$router = new Router;
$router->run();
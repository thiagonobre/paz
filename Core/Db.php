<?php
namespace Core;
class Db
{
    protected static $db;

    private function __construct($database_config)
    {
        //Pega a conexão no "config/database.php", referenciado no parametro:
        include_once __DIR__."/../Config/database.php";
        //Se não estiver passando o nome da conexão, conectará a primeira definida em "Config/database.php"
        $name_database = ($database_config) ? $database_config  : key($config["databases"]);
        $db_host = $config["databases"][$name_database]["host"];
        $db_nome = $config["databases"][$name_database]["database_name"];
        $db_usuario = $config["databases"][$name_database]["user"];
        $db_senha = $config["databases"][$name_database]["password"];
        $db_driver = $config["databases"][$name_database]["driver"];

        try
        {
            # Atribui o objeto PDO à variável $db.
            self::$db = new \PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
            # Garante que o PDO lance exceções durante erros.
            self::$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            # Garante que os dados sejam armazenados com codificação UFT-8.
            self::$db->exec('SET NAMES utf8');
        }
        catch (PDOException $e)
        {
            # Termina a execução mostrando o erro obtido na conexão.
            die("Connection Error: " . $e->getMessage());
        }
    }

    //Método estático - acessível sem instanciação. Singletone Pattern
    public static function connect($database_config)
    {
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$db)
        {
            new Db($database_config);
        }
        # Retorna a conexão.
        return self::$db;
    }

    public static function go($query, $modify = false, $every = true, $parameters = null){

        if (!self::$db) new Db('');
        
        //obter query do inventario
        $data = file_get_contents ("./src/Queries/_inventory.json");
        $inventory = json_decode($data, true);
        $file = $inventory[$query]['file'];
        if (empty($file)) die("[$query] não localizado em nosso inventário.");
        $instruction = file_get_contents ("./src/Queries/{$file}");
        $stmt = self::$db->prepare($instruction);

        if(!empty($parameters) )
        {
            foreach ($parameters as $key=>$value) 
            {
                $stmt->bindParam($key,$value);
            }
        }

        $execution = $stmt->execute();

        if (!$modify)
        {
            if ($every)
            {
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }else{
                return $stmt->fetch(\PDO::FETCH_ASSOC);
            }    
        }else{
            return $execution;
        }

    }

    


}